/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  02/03/2020 10:55:21                      */
/*==============================================================*/


drop table if exists ARTICLE;

drop table if exists CATEGORIE;

drop table if exists TRANSACTION;

drop table if exists UTILISATEUR;

/*==============================================================*/
/* Table : ARTICLE                                              */
/*==============================================================*/
create table ARTICLE
(
   ID_ARTICLE           int not null,
   NUM_TRANSACTION      int,
   ID_CATEGORIE         int not null,
   DESIGNATION          char(40),
   PRIX                 float,
   DESCRITPTION         longtext,
   PHOTO_ARTICLE        varchar(100),
   ETAT                 varchar(10),
   primary key (ID_ARTICLE)
);

/*==============================================================*/
/* Table : CATEGORIE                                            */
/*==============================================================*/
create table CATEGORIE
(
   ID_CATEGORIE         int not null,
   LIBELLE              varchar(40),
   primary key (ID_CATEGORIE)
);

/*==============================================================*/
/* Table : TRANSACTION                                          */
/*==============================================================*/
create table TRANSACTION
(
   NUM_TRANSACTION      int not null,
   ID_ARTICLE           int not null,
   ID_UTILISATEUR       int not null,
   DATE_TRANSACTION     date,
   TYPE_TRANSACTION     char(5),
   primary key (NUM_TRANSACTION)
);

/*==============================================================*/
/* Table : UTILISATEUR                                          */
/*==============================================================*/
create table UTILISATEUR
(
   ID_UTILISATEUR       int not null,
   NOM                  varchar(50),
   PRENOM               varchar(50),
   RUE                  varchar(150),
   CP                   int,
   VILLE                varchar(50),
   TELEPHONE            varchar(20),
   DATE_NAISSANCE       date,
   PHOTO_CLIENT         varchar(255),
   SEXE                 char(1),
   MAIL                 varchar(150),
   MOTS_DE_PASSE        varchar(50),
   primary key (ID_UTILISATEUR)
);

alter table ARTICLE add constraint FK_APPARTENIR foreign key (ID_CATEGORIE)
      references CATEGORIE (ID_CATEGORIE) on delete restrict on update restrict;

alter table ARTICLE add constraint FK_CONCERNER2 foreign key (NUM_TRANSACTION)
      references TRANSACTION (NUM_TRANSACTION) on delete restrict on update restrict;

alter table TRANSACTION add constraint FK_CONCERNER foreign key (ID_ARTICLE)
      references ARTICLE (ID_ARTICLE) on delete restrict on update restrict;

alter table TRANSACTION add constraint FK_EFFECTUER foreign key (ID_UTILISATEUR)
      references UTILISATEUR (ID_UTILISATEUR) on delete restrict on update restrict;

