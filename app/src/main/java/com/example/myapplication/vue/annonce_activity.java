package com.example.myapplication.vue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.controleur.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class annonce_activity extends AppCompatActivity {

    Button btn_accueil, btn_annonce, btn_enreg, btn_logout;
    EditText titre, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annonce);

        titre = (EditText)findViewById(R.id.title);
        description = (EditText)findViewById(R.id.desc);

        btn_accueil =(Button)findViewById(R.id.btn_accueil);
        btn_accueil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), main_activity.class);
                startActivity(i);
            }
        });

        btn_annonce =(Button)findViewById(R.id.btn_annoce);
        btn_annonce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), annonce_activity.class);
                startActivity(i);
            }
        });

        btn_logout =(Button)findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), login_activity.class);
                startActivity(i);
            }
        });

        btn_enreg =(Button)findViewById(R.id.btn_enreg);
        btn_enreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titreData = titre.getText().toString();
                String descriptionData = description.getText().toString();

                enregAnonce(titreData, descriptionData);
            }
        });

    }

    private void enregAnonce(String titre, String description){
        Call<ResponseBody> call = ApiClient.getInstance().getApi().saveAnnoncedata(titre, description);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Intent i = new Intent(getApplicationContext(), main_activity.class);
                startActivity(i);
                Toast.makeText(annonce_activity.this, "succes", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(annonce_activity.this, "error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
