package com.example.myapplication.vue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.modele.Annonce;
import com.example.myapplication.controleur.ApiClient;
import com.example.myapplication.outils.UserAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class main_activity extends AppCompatActivity {

    RecyclerView recyclerView;
    UserAdapter userAdapter;
    List<Annonce> annonce = new ArrayList<>();
    Button btn_accueil, btn_annonce, btn_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        btn_accueil =(Button)findViewById(R.id.btn_accueil);
        btn_accueil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), main_activity.class);
                startActivity(i);
            }
        });

        btn_annonce =(Button)findViewById(R.id.btn_annoce);
        btn_annonce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), annonce_activity.class);
                startActivity(i);
            }
        });

        btn_logout =(Button)findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), login_activity.class);
                startActivity(i);
            }
        });

        LoadData();
    }

    private void LoadData(){

        Call<List<Annonce>> listCall = ApiClient.getInstance().getApi().getAnnonce();
        listCall.enqueue(new Callback<List<Annonce>>() {
            @Override
            public void onResponse(Call<List<Annonce>> call, Response<List<Annonce>> response) {
                annonce = response.body();

                List<Annonce> annonces = response.body();
                userAdapter = new UserAdapter(main_activity.this, annonce);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onFailure(Call<List<Annonce>> call, Throwable throwable) {
                Toast.makeText(main_activity.this, "echec", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
