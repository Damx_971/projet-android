package com.example.myapplication.vue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.controleur.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class register_activity extends AppCompatActivity {

    //Composants
    EditText nom, prenom, email, mdp, rue, cp, ville, telephone, date_naiss;
    TextView connect_link;
    RadioGroup sexe;
    RadioButton homme, femme;
    Button btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        //initialisation
        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        email = (EditText) findViewById(R.id.emailLog);
        mdp = (EditText) findViewById(R.id.mdpLog);
        rue = (EditText) findViewById(R.id.rue);
        cp = (EditText) findViewById(R.id.cp);
        ville = (EditText) findViewById(R.id.ville);
        telephone = (EditText) findViewById(R.id.telephone);
        date_naiss = (EditText) findViewById(R.id.date_naiss);

        /*sexe = (RadioGroup) findViewById(R.id.sexe);
        homme = (RadioButton) findViewById(R.id.homme);
        femme = (RadioButton) findViewById(R.id.femme);
        int selectedRadioButtonID = sexe.getCheckedRadioButtonId();
        final RadioButton sexeValue = (RadioButton) findViewById(selectedRadioButtonID);
        sexeValue.getText().toString();*/

        connect_link = (TextView)findViewById(R.id.connect_link);
        connect_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), login_activity.class);
                startActivity(i);
            }
        });

        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nomData = nom.getText().toString();
                String prenomData = prenom.getText().toString();
                String emailData = email.getText().toString();
                String mdpData = mdp.getText().toString();
                String rueData = rue.getText().toString();
                String cpData = cp.getText().toString();
                String villeData = ville.getText().toString();
                String telephoneData = telephone.getText().toString();
                String date_naissData = date_naiss.getText().toString();
                /*String sexeData = sexeValue.getText().toString();*/

                savaData(nomData, prenomData, emailData, mdpData, rueData, cpData, villeData, telephoneData, date_naissData, "homme");
            }
        });
    }

    private void savaData(String nom, String prenom, String email, String mdp, String rue, String cp, String ville, String telephone, String date_naiss, String sexe) {
        Call<ResponseBody> call = ApiClient.getInstance().getApi().saveUserData(nom,prenom,email,mdp,rue,cp,ville,telephone,date_naiss,sexe);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(register_activity.this, "success", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), login_activity.class);
                startActivity(i);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Toast.makeText(register_activity.this, "error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

