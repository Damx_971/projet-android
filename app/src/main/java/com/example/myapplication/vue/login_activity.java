package com.example.myapplication.vue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.controleur.ApiClient;
import com.example.myapplication.modele.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login_activity extends AppCompatActivity {
    EditText emailLog, mdpLog;
    TextView register_link;
    Button btn_connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        emailLog = (EditText) findViewById(R.id.emailLog);
        mdpLog = (EditText) findViewById(R.id.mdpLog);

        register_link = (TextView) findViewById(R.id.register_link);
        register_link.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), register_activity.class);
                startActivity(i);
            }
        });

        btn_connect = (Button)findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = emailLog.getText().toString();
                String password = mdpLog.getText().toString();

                UserLogin(login, password);
            }
        });
    }

    private void UserLogin(String email, String mdp){

        if(email.isEmpty()){
            emailLog.setError("Email invalide");
            emailLog.requestFocus();
            return;
        }

        if(mdp.isEmpty()){
            mdpLog.setError("Mots de passe invalide");
            mdpLog.requestFocus();
            return;
        }

        Call<List<LoginResponse>>call = ApiClient.getInstance().getApi().getReponse(email, mdp);
        call.enqueue(new Callback<List<LoginResponse>>() {

            @Override
            public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {
                List<LoginResponse> posts = response.body();

                Boolean ss =posts.get(0).getSucess();

                if(ss.equals(true)){
                    Intent i = new Intent(getApplicationContext(), main_activity.class);
                    startActivity(i);
                }
                else{
                    Toast.makeText(login_activity.this, "Login ou mots de passe incorrecte", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<LoginResponse>> call, Throwable throwable) {
                Toast.makeText(login_activity.this, "echec", Toast.LENGTH_SHORT).show();
            }

        });

    }
}
