package com.example.myapplication.outils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.modele.Annonce;
import com.example.myapplication.modele.LoginResponse;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.RecyclerHolder> {

    Context context;
    List<Annonce> annonce;

    public UserAdapter(Context context, List<Annonce> annonce) {
        this.context = context;
        this.annonce = annonce;
    }

    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item,parent, false);
        return new RecyclerHolder(view );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {
        Annonce annonces = annonce.get(position);
        holder.tvTitle.setText(annonces.getTitre());
        holder.tvDesc.setText(annonces.getDescription());
    }

    @Override
    public int getItemCount() {
        return annonce.size();
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDesc;
        RecyclerHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.title);
            tvDesc = itemView.findViewById(R.id.desc);
        }
    }
}
