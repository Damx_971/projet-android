package com.example.myapplication.outils;

import com.example.myapplication.modele.Annonce;
import com.example.myapplication.modele.LoginResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("saveUser.php")
    Call<ResponseBody>saveUserData(
            @Field("nom") String nom,
            @Field("prenom") String prenom,
            @Field("email") String email,
            @Field("mdp") String mdp,
            @Field("rue") String rue,
            @Field("cp") String cp,
            @Field("ville") String ville,
            @Field("telephone") String telephone,
            @Field("date_naiss") String date_naiss,
            @Field("sexe") String sexe
    );

    @FormUrlEncoded
    @POST("saveAnnonce.php")
    Call<ResponseBody>saveAnnoncedata(
            @Field("titre") String titre,
            @Field("description") String description
    );

   @GET("login.php")
    Call<List<LoginResponse>> getReponse(
           @Query("email") String email,
           @Query("mdp") String mdp
   );

    @GET("liste.php")
    Call<List<Annonce>> getAnnonce();
}
