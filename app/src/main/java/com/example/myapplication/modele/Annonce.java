package com.example.myapplication.modele;

import java.util.Date;

public class Annonce {
    private String titre, description;

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }
}
